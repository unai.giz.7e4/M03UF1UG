﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Pide al usuario un numero y hace una cuenta hacia atras. 
 */

namespace CountDown
{
    class CountDown
    {
        static void Main(string[] args)
        {
            // User Input.
            Console.Write("Hand over a number.\nINPUT> ");
            int num = Convert.ToInt32(Console.ReadLine());

            for (; num != 0; --num)
            {
                Console.Write($"{num} ");
            }
        }
    }
}
