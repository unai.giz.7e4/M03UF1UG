﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Comprueba si el numero que ha introducido el usuario esta entre 1 y 5.
 */

namespace NumberBetweenOneAndFive
{
    class NumberBetweenOneAndFive
    {
        static void Main(string[] args)
        {
            // While loop.
            int number = 0;
            while (number < 1 || number > 5)
            {
                // User input
                Console.Write("Hand over a number (Between 1-5).\nINPUT> ");
                number = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine($"Your number is {number}");
        }
    }
}
