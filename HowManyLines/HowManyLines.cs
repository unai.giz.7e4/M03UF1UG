﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Cuenta el total de lineas que el usuario ha introducido.
 */

namespace HowManyLines
{
    class HowManyLines
    {
        static void Main(string[] args)
        {
            // Declaring & Console.
            int count = 0;
            string lines = "";
            Console.WriteLine($"Send any text you'd like, return 'END' to exit.");

            // While loop.
            while (lines != "END") // lines.ToLower() != "end"
            {
                // User input
                lines = Console.ReadLine();

                // Check if the string was empty
                //if (lines.Trim() != string.Empty)
                //{
                    count++;
                //}
            }
            // Writing the lines the user sent.
            Console.WriteLine($"You sent in {count-1} lines.");
        }
    }
}
