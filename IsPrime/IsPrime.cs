﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 24/10/2022.
 * Descripcion: Un programa que mostre true si el nombre introduït és primer, false en qualsevol altre cas.
 */

namespace IsPrime
{
    internal class IsPrime
    {
        static void Main(string[] args)
        {
            // User Input
            Console.Write($"Number?\nINPUT> ");
            int num = Convert.ToInt32(Console.ReadLine());

            bool IsPrime = true;
            for (int counter = 2; counter < num; counter++)
            {
                if (num % counter == 0)
                {
                    IsPrime = false;
                    break;
                }
            }
            if (IsPrime)
            {
                Console.WriteLine("Es primo");
            }
            else
            {
                Console.WriteLine("No es primo");
            }
        }
    }
}
