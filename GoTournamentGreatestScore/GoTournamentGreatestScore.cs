﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Pide al usuario un nombre y puntuación, si es mas grande se actualiza para mostrar al ganador.
 */

namespace GoTournamentGreatestScore
{
    class GoTournamentGreatestScore
    {
        static void Main(string[] args)
        {
            int bestScore = 0;
            string bestPlayer = "None";
            int playerCount = 0;

            while (true)
            {
                playerCount++;

                // Player Name.
                Console.Write($"{playerCount}) Player name? ('END' to stop)\nINPUT> ");
                string player = Console.ReadLine();

                // Break loop?
                if (player == "END")
                {
                    break;
                }

                // Ask For score
                Console.Write($"{playerCount}) Player score?\nINPUT> ");
                int score = Convert.ToInt32(Console.ReadLine());

                // Check for score and replace winner
                if (score > bestScore)
                {
                    bestPlayer = player;
                    bestScore = score;
                }

            }
            Console.WriteLine($"{bestPlayer}: {bestScore}");
        }
    }
}