﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 24/10/2022.
 * Descripcion: Fibonacci.
 */

namespace Fibonacci
{
    internal class Fibonacci
    {
        static void Main(string[] args)
        {
            // User Input
            Console.Write($"Number?\nINPUT> ");
            int original = Convert.ToInt32(Console.ReadLine());

            int num = original;
            for (int i = 0; i < original; i++)
            {
                num = num - 1 + num - 2; // This is retarded

                if (num <= original)
                {
                    Console.WriteLine(num);
                }
            }
        }
    }
}
