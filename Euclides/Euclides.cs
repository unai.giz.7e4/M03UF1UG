﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 24/10/2022.
 * Descripcion: Implementa l'algorisme d'Euclides, que calcula le màxim comú divisor de dos nombres positius.
 */

namespace Euclides
{
    internal class Euclides
    {
        static void Main(string[] args)
        {
            // User Input
            Console.Write($"First number?\nINPUT> ");
            int numOne = Convert.ToInt32(Console.ReadLine());

            // User Input
            Console.Write($"Second number?\nINPUT> ");
            int numTwo = Convert.ToInt32(Console.ReadLine());

            int res = 0;
            do
            {
                res = (numOne % numTwo);
                numOne = numTwo;
                numTwo = res;

                Console.WriteLine(numOne);
            } while (res != 0);
        }
    }
}