﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 24/10/2022.
 * Descripcion: Crea una piramide con la altura que el usuario especifica (centrada)
 */

namespace CenterPiramid
{
    class CenterPiramid
    {
        static void Main(string[] args)
        {
            // User Input
            Console.Write("Hand over a number.\nINPUT> ");
            int numOne = Convert.ToInt32(Console.ReadLine());

            // Nested For Loops.
            for (int counter = 1; counter <= numOne; counter++)
            {
                // For Loop (Spacing)
                for (int c3 = 1; c3 < numOne-counter; c3++)
                {
                    Console.Write(" ");
                }
                
                // For Loop (#)
                for (int c2 = 0; c2 < counter; c2++)
                {
                    Console.Write("# ");
                }

                // Update Pyramid's Current Line.
                Console.WriteLine();
            }
            // Total.
            Console.Write($"Total: {numOne}x{numOne} pyramid.");
        }
    }
}
