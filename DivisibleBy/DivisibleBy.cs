﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 24/10/2022.
 * Descripcion: Imprimeix tots els nombres enters divisibles per 3 que hi ha entre A i B (inclusiu).
 */

namespace DivisibleBy
{
    internal class DivisibleBy
    {
        static void Main(string[] args)
        {
            // User Input
            Console.Write($"First number?\nINPUT> ");
            int numOne = Convert.ToInt32(Console.ReadLine());

            Console.Write($"Second number?\nINPUT> ");
            int numTwo = Convert.ToInt32(Console.ReadLine());

            // For Loop
            for (int currNum = numOne; currNum <= numTwo; currNum++)
            {
                int op = (currNum % 3);
                if (op == 0)
                {
                    Console.WriteLine(currNum);
                }
            }
        }
    }
}
