﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Espera hasta que el usuario escriba el numero 5.
 */

namespace WaitFor5
{
    class WaitFor5
    {
        static void Main(string[] args)
        {
            // While loop.
            int number = 0;
            while (number != 5) 
            { 
                // User input
                Console.Write("Hand over a number.\nINPUT> ");
                number = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("5 trobat!");
        }
    }
}
