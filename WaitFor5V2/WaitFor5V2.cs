﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Espera hasta que el usuario escriba el numero 5.
 */

namespace WaitFor5V2
{
    class WaitFor5V2
    {
        static void Main(string[] args)
        {
            // Do loop.
            int number = 0;
            do
            {
                // User input
                Console.Write("Hand over a number.\nINPUT> ");
                number = Convert.ToInt32(Console.ReadLine());
            } while (number != 5) ;
                Console.WriteLine("5 trobat!");
        }
    }
}
