﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Cuenta hasta 1 (hacia atras) con el numero que el usuario introduce.
 */

namespace LetsCount
{
    class LetsCount
    {
        static void Main(string[] args)
        {
            // User input.
            Console.Write("Hand over a number.\nINPUT> ");
            int number = Convert.ToInt32(Console.ReadLine());

            // While loop
            while (number > 0) 
            {
                Console.WriteLine($"{number}");
                --number;
            }
        }
    }
}
