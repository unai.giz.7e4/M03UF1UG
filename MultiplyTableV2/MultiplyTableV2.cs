﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Enseña la tabla de multiplicar con el numero que el usuario especifica.
 */

namespace MultiplyTableV2
{
    class MultiplyTableV2
    {
        static void Main(string[] args)
        {
            // User input.
            Console.Write("Hand over a number.\nINPUT> ");
            int number = Convert.ToInt32(Console.ReadLine());

            // Do loop
            int counter = 1;
            do
            {
                Console.WriteLine($"{counter} * {number} = {counter * number}");
                counter++;
            } while (counter <= 10);
        }
    }
}
