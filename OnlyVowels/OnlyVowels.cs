﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Muestra las vocales, solo vocales.
 */

namespace OnlyVowels
{
    class OnlyVowels
    {
        static void Main(string[] args)
        {
            // User Input
            Console.Write("Hand over a number.\nINPUT> ");
            int limit = Convert.ToInt32(Console.ReadLine());
                
            for (int counter = 0; counter < limit; counter += 1)
            {
                Console.Write("INPUT> ");
                string letter = Console.ReadLine(); //.Read no puede convertir ASCII a letra (que yo sepa) y no voy a poner 20 cases con Console.WriteLine(...

                switch (letter) //.ToLower() o .ToLower()
                {
                    case "a": case "A": case "e": case "E": case "i": case "I": case "o": case "O": case "u": case "U":
                        Console.WriteLine($"{letter} is a vowel.");
                        break;

                    default:
                        Console.WriteLine($"{letter} is not a vowel");
                        break;
                }
            }

            /*
            // Iterate trough (not a bad idea).
            for (int counter = 0; counter < limit*2; counter+=2) // I would love to use this with a list.
            {
                char letter = characters[counter];
                //Console.WriteLine(letter.ToString());
                if (letter.ToString().ToLower() == "a") 
                {
                    Console.WriteLine($"{letter} ");
                }
            }
            */

            /*
            foreach (char letter in characters) // I would love to use this with a list (or .lower()).
            {
                if (letter == Convert.ToChar("a") || letter == Convert.ToChar("A") || letter == Convert.ToChar("e") || letter == Convert.ToChar("E") || letter == Convert.ToChar("i") || letter == Convert.ToChar("I") || letter == Convert.ToChar("o") || letter == Convert.ToChar("O") || letter == Convert.ToChar("u") || letter == Convert.ToChar("U"))
                {
                    Console.WriteLine($"{letter}");
                }
            }
            */
        }
    }
}