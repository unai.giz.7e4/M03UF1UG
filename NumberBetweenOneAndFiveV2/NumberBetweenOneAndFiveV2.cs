﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Comprueba si el numero que ha introducido el usuario esta entre 1 y 5.
 */

namespace NumberBetweenOneAndFiveV2
{
    class NumberBetweenOneAndFiveV2
    {
        static void Main(string[] args)
        {
            // Do loop.
            int number = 0;
            do
            {
                // User input
                Console.Write("Hand over a number (Between 1-5).\nINPUT> ");
                number = Convert.ToInt32(Console.ReadLine());
            } while (number < 1 || number > 5);
            Console.WriteLine($"Your number is {number}");
        }
    }
}
