﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Suma los digitos de un numero entero caracter por caracter.
 */

namespace SumMyNumbers
{
    class SumMyNumbers
    {
        static void Main(string[] args)
        {
            // User Input
            Console.Write("Hand over a number.\nINPUT> ");
            int numOne = Convert.ToInt32(Console.ReadLine());
            int opera = 0;

            while (numOne > 0)
            {
                opera = opera + (numOne % 10);
                numOne = numOne / 10;
            }
            Console.WriteLine(opera);
        }
    }
}
