﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Pide dos numeros y cuenta los numeros que hay de por medio.
 */

namespace LetsCountBetween
{
    class LetsCountBetween
    {
        static void Main(string[] args)
        {
            // User input.
            Console.Write("Hand over a number.\nINPUT> ");
            int numOne = Convert.ToInt32(Console.ReadLine());

            Console.Write("Hand over a number.\nINPUT> ");
            int numTwo = Convert.ToInt32(Console.ReadLine());

            // Change numbers if invalid.
            if (numOne > numTwo)
            {
                (numOne, numTwo) = (numTwo, numOne);
            }

            // For Loop.
            for (; numOne < numTwo-1; numOne++)
            {
                Console.Write($"{numOne+1} ");
            }
        }
    }
}
