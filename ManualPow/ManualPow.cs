﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Math.Pow() sin utilizarlo.
 */

namespace ManualPow
{
    class ManualPow
    {
        static void Main(string[] args)
        {
            // User Input.
            Console.Write("Hand over a number.\nINPUT> ");
            int numOne = Convert.ToInt32(Console.ReadLine());

            Console.Write("Times to multiply with?\nINPUT> ");
            int numTwo = Convert.ToInt32(Console.ReadLine());

            int counter = numOne;
            // Hell.
            for (; numTwo > 1; --numTwo)
            {
                counter = counter * numOne;
            }
            Console.WriteLine($"{counter}");
        }
    }
}
