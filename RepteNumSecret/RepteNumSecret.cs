﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: "Juego" en el que el jugador tiene que adivinar el numero secreto generado aleatoriamente, tiene 10 vidas/intentos.
 */ 

namespace RepteNumSecret
{
    class RepteNumSecret
    {
        static void Main(string[] args)
        {
            // Getting the random number.
            Random rand = new Random();
            int secretNum = rand.Next(1, 101);
            int guess = 0;
            int tries = 10;

            // Do-While Loop.
            while (guess != secretNum && tries != 0)
            {
                // User Area && Subtracting try
                Console.Write($"Tries left: {tries}\nGuess the number.\nINPUT> ");
                guess = Convert.ToInt32(Console.ReadLine());
                --tries;

                // Checking for the number. 
                Console.Clear();
                if (guess > secretNum)
                {
                    Console.WriteLine($"The number is lower than {guess}!");
                }
                else if (guess < secretNum)
                {
                    Console.WriteLine($"The number is higher than {guess}!");
                }
            }
            // End of the game
            if (tries == 0 && guess != secretNum)
            {
                Console.WriteLine($"Whoops, you ran out of tries. The number was {secretNum}.");
            }
            else
            {
                Console.WriteLine($"You win! The number was {secretNum}.");
            }
        }
    }
}
