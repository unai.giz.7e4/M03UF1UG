﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 24/10/2022.
 * Descripcion: Calcula el factorial de un numero.
 */

namespace Factorial
{
    internal class Factorial
    {
        static void Main(string[] args)
        {
            // User Input
            Console.Write($"Number?\nINPUT> ");
            int num = Convert.ToInt32(Console.ReadLine());

            // Factorial
            int calc = num;
            for (int i = 1; i < num; i++)
            {
                calc = calc * i;
            }
            Console.WriteLine(calc);
        }
    }
}
