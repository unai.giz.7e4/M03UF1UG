﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Cuenta hasta 1 (hacia atras) con el numero que el usuario introduce.
 */

namespace LetsCount
{
    class LetsCountV2
    {
        static void Main(string[] args)
        {
            // User input.
            Console.Write("Hand over a number.\nINPUT> ");
            int number = Convert.ToInt32(Console.ReadLine());

            // Do loop
            do 
            {
                Console.WriteLine($"{number}");
                --number;
            } while (number > 0);
        }
    }
}
