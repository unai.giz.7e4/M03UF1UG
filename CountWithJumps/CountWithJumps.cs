﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Pide dos numeros, y va sumando hasta llegar al segundo numero.
 */

namespace CountWithJumps
{
    class CountWithJumps
    {
        static void Main(string[] args)
        {
            // User Input.
            Console.Write("Hand over a number.\nINPUT> ");
            int numOne = Convert.ToInt32(Console.ReadLine());

            Console.Write("Hand over a number.\nINPUT> ");
            int numTwo = Convert.ToInt32(Console.ReadLine());

            int race = 1;
            // Do.
            while (race < numOne+3)
            {
                Console.WriteLine($"{race}");
                race += numTwo;
            }
        }
    }
}
