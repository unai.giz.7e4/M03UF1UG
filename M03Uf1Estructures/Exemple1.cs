﻿using System;

namespace M03Uf1Estructures
{
    class Exemple1
    {
        static void Main(string[] args)
        {
            int num = 0;
            while (num != 100000) 
            {
                Console.WriteLine($"The number is: {num+1}");
                num++;
            }
        }
    }
}