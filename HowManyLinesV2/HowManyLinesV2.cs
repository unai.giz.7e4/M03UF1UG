﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Cuenta el total de lineas que el usuario ha introducido.
 */

namespace HowManyLines
{
    class HowManyLinesV2
    {
        static void Main(string[] args)
        {
            // Declaring & Console.
            int count = 0;
            string lines = "";
            Console.WriteLine($"Send any text you'd like, return 'END' to exit.");

            // Do loop.
            do 
            {
                // User input.
                lines = Console.ReadLine();
                count++;

            } while (lines != "END");

            // Writing the lines the user sent.
            Console.WriteLine($"You sent in {count - 1} lines.");
        }
    }
}
