﻿using System;

namespace ToBinnary
{
    class ToBinnary
    {
        static void Main(string[] args)
        {
            // User Input.
            Console.Write($"Give me an integer.\nINPUT> ");
            int number = Convert.ToInt32(Console.ReadLine());

            // Do loop.
            do
            {
                // Calculating.
                int res = number % 2;
                number = number / 2;

                // Printing reverse binary.
                if (res == 0)
                {
                    Console.Write(1);
                }
                else
                {
                    Console.Write(0);
                }
            } while (number != 0);
        }
    }
}
