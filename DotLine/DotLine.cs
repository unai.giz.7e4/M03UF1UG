﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Console.Write(".");
 */

namespace DotLine
{
    class DotLine
    {
        static void Main(string[] args)
        {
            // User input.
            Console.Write("Hand over a number.\nINPUT> ");
            int num = Convert.ToInt32(Console.ReadLine());

            // For Loop.
            for (int i = 0; i < num; i++)
            {
                Console.Write(".");
            }
        }
    }
}
