﻿using System;

/*
 * Autor: Unai Giz Navarro.
 * Data: 19/10/2022.
 * Descripcion: Muestra la tabla de multiplicar.
 */

namespace MultiplyTableFull
{
    class MultiplyTableFull
    {
        static void Main(string[] args)
        {
            // For.
            for (int counter = 1; counter <=9; counter++)
            {
                Console.WriteLine($"{counter} | {1*counter} | {2 * counter} | {3 * counter} | {4 * counter} | {5 * counter} | {6 * counter} | {7 * counter} | {8 * counter} | {9 * counter} |\n----------------------------------------------");
            }
        }
    }
}
